# https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#The-Set-Builtin
set -o errexit
set -o verbose
rm -f database.db
sqlite3 database.db ".read database-schema.sql"
sqlite3 database.db ".read test-data.sql"
