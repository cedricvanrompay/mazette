import { createLabSession } from '$lib/database';
import { fail, redirect } from '@sveltejs/kit';
import type { Actions } from './$types';

export const actions = {
    default: async ({ request, locals: { account }}) => {
        const data = await request.formData();
        
        const title = data.get('title') as string | null;
        const description = data.get('description') as string | null;

        if (!title) {
            return fail(400, { description, missingTitle: true });
        }

        const id = createLabSession(title, description, account);

        throw redirect(303, `/lab-sessions/${id}`);
    }
} satisfies Actions;