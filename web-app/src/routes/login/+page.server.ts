import { login, LoginFailed } from '$lib/database';
import { fail, redirect } from '@sveltejs/kit';
import type { Actions } from './$types';
 
export const actions = {
  default: async ({ request, cookies }) => {
    const data = await request.formData();
    const email  = data.get('email') as string | null;
    const password = data.get('password') as string | null;

    if (!email || !password) {
			return fail(400, { email, missing: true });
		}

    let sessionCookie: string;
    try {
      sessionCookie = await login(email, password);
      cookies.set('session', sessionCookie);
    } catch (error) {
      if (error instanceof LoginFailed) {
        return fail(400, {email, incorrect: true})
      }

      throw error;
    }

    throw redirect(303, '/account');
  }
} satisfies Actions;
