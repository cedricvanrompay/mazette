import { createAccount, createSessionCookie, deleteAccountCreationToken, getAccountCreationToken } from '$lib/database';
import { fail, redirect } from '@sveltejs/kit';
import type { Actions } from './$types';
 
export const actions = {
  default: async ({ request, cookies }) => {
    const data = await request.formData();

    const receivedToken = data.get('token') as string | null;
    const email  = data.get('email') as string | null;

    if (!receivedToken) {
        return fail(400, {email, noToken: true})
    }

    const dbToken = getAccountCreationToken(receivedToken);

    if (!dbToken) {
        return fail(400, {invalidToken: true})
    }

    const password = data.get('password') as string | null;

    if (!email || !password) {
        return fail(400, { email, missing: true });
    }

    const id = await createAccount(email, password, dbToken.info);

    deleteAccountCreationToken(dbToken.value);

    const sessionCookie = createSessionCookie(id);
    cookies.set('session', sessionCookie);

    throw redirect(303, '/#');
  }
} satisfies Actions;