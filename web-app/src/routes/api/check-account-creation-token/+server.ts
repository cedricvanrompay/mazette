import { getAccountCreationToken } from '$lib/database';
import { error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const POST: RequestHandler = async ({ request }) => {
    const token = await request.text();

    const tokenData = getAccountCreationToken(token);

    if (tokenData) {
        return new Response();
    } else {
        throw error(403);
    }
};