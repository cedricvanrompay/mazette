export type LabSession = {
    id: string
    title: string
    created_at: string
    join_code?: string
    joinable_until?: string
    comments?: string
}

export type LabStudentSession = {
    id: string
    created_at: string
    lab_session_id: string
    last_saved_at: string
}

export type Account = {
    id: string
    email: string
    created_at: string
}

export type AccountCreationToken = {
    value: string
    created_at: string
    info: string
}