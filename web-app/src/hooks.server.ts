import type { Handle } from '@sveltejs/kit';
import { getAccountFromCookie, getLabStudentSessionFromCookie, InvalidCookie } from '$lib/database';
import { Forbidden, Unauthorized } from '$lib/statusCodes';

const publicPaths = new Set([
    '/login',
    '/register',
    '/join-session',
]);

const labSessionApiPaths = new Set([
    '/api/student-session',
    '/api/editor-snapshot',
])

export const handle = (async ({ event, resolve }) => {
    if (labSessionApiPaths.has(event.url.pathname)) {
        const labSessionCookie = event.cookies.get('labStudentSession');

        if (labSessionCookie) {
            try {
                event.locals.labStudentSession = getLabStudentSessionFromCookie(labSessionCookie);
            } catch (error) {
                return new Response(null, { status: Forbidden });
            }
        } else {
            return new Response(null, { status: Unauthorized });
        }
    } else if (!publicPaths.has(event.url.pathname)) {
        const sessionCookie = event.cookies.get('session');
        if (sessionCookie) {
            try {
                event.locals.account = getAccountFromCookie(sessionCookie);
            } catch (error) {
                if (error instanceof InvalidCookie) {
                    return Response.redirect(`${event.url.origin}/login`, 303);
                }

                throw error;
            }
        } else {
            return Response.redirect(`${event.url.origin}/login`, 303);
        }
    }

    const response = await resolve(event);

    return response;
}) satisfies Handle;
