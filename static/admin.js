const deleteStudentForm = document.getElementById('delete-student-form');
if (deleteStudentForm) {
    const studentId = deleteStudentForm.dataset.studentId;

    deleteStudentForm.onsubmit = (event) => {
        const msg = `Supprimer l'étudiant ${studentId} ?`;
        const confirmed = window.confirm(msg);
        if (!confirmed) {
            event.preventDefault();
        }
    }
}