INSERT INTO accounts(
    id,
    email,
    created_at,
    info
) VALUES (
    '4fd26af6afe2d809',
    'test@email.com',
    '2023-03-25T14:17:15.509Z',
    'populated by test-data.sql'
), (
    'af6fd26af6afe2d8c',
    'test2@email.com',
    '2023-03-25T14:17:15.509Z',
    'populated by test-data.sql'
);

INSERT INTO account_passwords(
    account_id,
    hash
) VALUES (
    '4fd26af6afe2d809',
    '$argon2id$v=19$m=65536,t=3,p=4$U9E4oir1l9gX7nKco9fKCA$OgZhv58CJnpMtRfPKq6YNQf26o8Upu/9Jf1A/7JDhEk' -- "password"
);

INSERT INTO lab_sessions(
    id,
    title,
    created_at,
    teacher_account_id,
    join_code,
    joinable_until,
    comments
) VALUES (
    'abc',
    'test session 1',
    '2023-03-19T12:34:58.344Z',
    '4fd26af6afe2d809',
    'c78a',
    '2023-03-19T16:34:58.344Z',
    'test comment'
), (
    'def',
    'test session 2',
    '2023-03-19T13:34:58.344Z',
    '4fd26af6afe2d809',
    'v9c8',
    '2023-03-19T16:14:58.344Z',
    'test comment 2'
), (
    'ghi',
    'test session 3',
    '2023-03-19T13:44:58.344Z',
    'af6fd26af6afe2d8c',
    null,
    null,
    ''
); 

INSERT INTO lab_student_sessions (
    id,
    created_at,
    lab_session_id,
    last_save,
    last_saved_at
) VALUES (
    '6f254',
    '2023-03-19T12:44:58.344Z',
    'abc',
    '# this is a test save
def f(): print("hi")',
    '2023-03-19T13:44:58.344Z'
), (
    'c4d9e',
    '2023-03-19T12:46:58.344Z',
    'abc',
    null,
    null
);