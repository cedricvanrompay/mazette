This repository contains both the first lab and the beginning of a teacher interface.

Running `setup.sh` will install all the dependencies of the lab in `static/tp/01`, except the video (which, for now, should be dowloaded from the Internet? TODO improve this). You should be able to serve this directory as a standalone website after this (for instance, `cd static/tp/01 && python -m http.server`).


## Teacher Interface

This is still very much a work in progress.

To run the teacher interface:
* run `bash reset-database.sh`
* start the web app (see the instructions in the `web-app` directory)
* start the Nginx reverse proxy: `docker-compose up --build`

You can then access the public website at http://localhost:8057/ and the login page to the teacher interface at http://localhost:8057/login.
